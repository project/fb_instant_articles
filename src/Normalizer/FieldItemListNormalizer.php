<?php

namespace Drupal\fb_instant_articles\Normalizer;

use Drupal\Core\Field\FormatterInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\fb_instant_articles\Plugin\Field\InstantArticleFormatterInterface;
use Drupal\fb_instant_articles\Regions;
use Drupal\fb_instant_articles\TransformerFactory;
use Drupal\serialization\Normalizer\NormalizerBase;
use Facebook\InstantArticles\Elements\Footer;
use Facebook\InstantArticles\Elements\Header;

/**
 * Normalize FieldItemList object into an Instant Article object.
 */
class FieldItemListNormalizer extends NormalizerBase {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = 'Drupal\Core\Field\FieldItemListInterface';

  /**
   * {@inheritdoc}
   */
  protected $format = 'fbia';

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Transformer factory.
   *
   * @var \Drupal\fb_instant_articles\TransformerFactory
   */
  protected $transformerFactory;

  /**
   * FieldItemListNormalizer constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\fb_instant_articles\TransformerFactory $transformer_factory
   *   Transformer factory.
   */
  public function __construct(RendererInterface $renderer, TransformerFactory $transformer_factory) {
    $this->renderer = $renderer;
    $this->transformerFactory = $transformer_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []) {
    /** @var \Drupal\Core\Field\FieldItemListInterface $object */
    if (!isset($context['instant_article'])) {
      return;
    }
    /** @var \Facebook\InstantArticles\Elements\InstantArticle $article */
    $article = $context['instant_article'];

    // If we're given an entity_view_display object as context, use that as a
    // mapping to guide the normalization.
    if (isset($context['entity_view_display'])) {
      /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $display */
      $display = $context['entity_view_display'];
      $formatter = $display->getRenderer($object->getName());
      $component = $display->getComponent($object->getName());
      if ($formatter instanceof InstantArticleFormatterInterface) {
        $formatter->viewInstantArticle($object, $article, $component['region'], $this->serializer);
      }
      elseif ($formatter instanceof FormatterInterface) {
        $formatter->prepareView([$object->getEntity()->id() => $object]);
        $render_array = $formatter->view($object);
        if ($markup = (string) $this->renderer->renderPlain($render_array)) {

          // Determine correct context for transformation.
          $transformer_context = $article;
          if ($component['region'] === Regions::REGION_HEADER) {
            $header = $article->getHeader();
            if (!$header) {
              $header = Header::create();
              $article->withHeader($header);
            }
            $transformer_context = $header;
          }
          elseif ($component['region'] === Regions::REGION_FOOTER) {
            $footer = $article->getFooter();
            if (!$footer) {
              $footer = Footer::create();
              $article->withFooter($footer);
            }
            $transformer_context = $footer;
          }

          // Region-aware transformation of rendered markup.
          // Pass the markup through the Transformer.
          $transformer = $this->transformerFactory->getTransformer();
          $transformer->transformString($transformer_context, $markup);
          $this->transformerFactory->flushTransformerLogs($transformer);
        }
      }
    }
  }

}
